## Nuxt Config YML

Use YAML for nuxt.config

### Install

```
npm i https://gitlab.com/DRogueRonin/nuxt-config-yml
```

Add to `nuxt.config.js` modules

```
modules: [
    'nuxt-config-yml'
]
```

### Configure

You can overwrite the yml config file's name

Add to `nuxt.config.js`

```
nuxtConfigYml: {
    configFile: 'some.other.nuxt.config.yaml'
}
```
